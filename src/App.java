import models.Circle;
import models.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
       Circle circle = new Circle(2.5);
       System.out.println("Circle: ");
       System.out.println(circle.getPerimeter());
       System.out.println(circle.getArea());
       System.out.println(circle.toString());
       System.out.println("-----------------------------------");


       ResizableCircle resizable = new ResizableCircle(2.5);
       System.out.println("Resizable: ");
       System.out.println(resizable.getPerimeter());
       System.out.println(resizable.getArea());
       System.out.println(resizable.toString());
       System.out.println("-----------------------------------");


       System.out.println("Vùng Resize:");
       resizable.resize(10);
       System.out.println(resizable.toString());
    }
}
