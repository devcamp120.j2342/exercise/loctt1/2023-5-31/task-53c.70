package interfaces;

public interface GeometricObjectClassV2 {
     public double getPerimeter();
     public double getArea();
}
