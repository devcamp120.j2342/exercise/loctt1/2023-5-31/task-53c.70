package models;

import interfaces.GeometricObjectClassV2;

public class Circle implements GeometricObjectClassV2{
     protected double radius;

     public Circle(double radius) {
          this.radius = radius;
     }

     @Override
     public String toString() {
          return "Circle [radius=" + this.radius + "]";
     }

     @Override
     public double getPerimeter() {
          return 2 * Math.PI * radius;
     }

     @Override
     public double getArea() {
          return  Math.PI * Math.pow(radius, 2);
     }
}
